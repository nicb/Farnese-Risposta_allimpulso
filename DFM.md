# Definitive Farina Method

Metodo di realizzazione della risposta all'impulso così come descritto a voce
dal Prof.Angelo Farina dell'Università di Parma la sera della prima del
_Prometeo_ di Luigi Nono al Teatro Farnese il 26 maggio 2017.

## Passi metodologici

1. realizzare uno segnale sintetico cosinusoidale di sweep (20 Hz - 20480 Hz)
   ad ampiezza costante (-6 dB)
1. emettere il segnale sintetico con altoparlanti e riprendere la risposta del
   luogo con microfoni (vedi specifica)
1. convolvere la risposta registrata col segnale sintetico rovesciato
   equalizzato verso l'acuto con un aumento di 6 dB/ottava
1. finestrare l'impulso al fine di rimuovere i picchi sul lato sinistro che
   sono i risultati di distorsioni di armoniche superiori provocate dalla
   risposta non-lineare degli altoparlanti.

Alla fine di questi passi si ottiene la risposta all'impulso corredata di
modulo e fase su tutte le frequenze prese in considerazione.

Per una descrizione più approfondità del metodo vedi:
https://www.researchgate.net/publication/2456363_Simultaneous_Measurement_of_Impulse_Response_and_Distortion_With_a_Swept-Sine_Technique
