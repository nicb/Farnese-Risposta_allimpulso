(
fork{
	var
	folders,
	file,
	written,
	totFiles,
	numSpeakers = 8,
	numMics = 5,
	distances12 = Array.readArchive(thisProcess.nowExecutingPath.dirname ++ "/../docs/distances_Mics12_ls.txt"),
	distances34 = Array.readArchive(thisProcess.nowExecutingPath.dirname ++ "/../docs/distances_Mics34_ls.txt"),
	distances5 = Array.readArchive(thisProcess.nowExecutingPath.dirname ++ "/../docs/distances_Mics5_ls.txt"),
	c = Condition.new;

	totFiles = numSpeakers*numMics;

	numMics.do({
		|i|
		folders = thisProcess.nowExecutingPath.dirname ++ "/new_mic";
		File.mkdir(folders ++ (i+1));
	});

	s.sync;

	totFiles.do{
		|i|
		var
		path,
		writePath,
		lsNumber = ((i)%numSpeakers)+1,
		micNumber = ((i/numSpeakers).asInteger)+1;
		path = thisProcess.nowExecutingPath.dirname ++ "/mic"++ micNumber ++ "/CONVls" ++ lsNumber ++ "-00" ++micNumber++".wav";

		Buffer.freeAll;

		s.sync;

		a = Buffer.read(s, path, action: {c.unhang});

		c.hang;

		a.loadToFloatArray(0, -1, {|arr|
			var
			cursor,
			array,
			marker,
			start,
			distance,
			window,
			peak = arr.maxIndex;

			if(micNumber < 3,
				{distance = distances12[lsNumber-1]},
				{if(micNumber == 5,
					{distance = distances5[lsNumber-1]},
					{distance = distances34[lsNumber-1]});
			});

			cursor = peak - (s.sampleRate*distance);

			array = Array.fill(arr.size-(cursor), {
				|i|
				arr[(i+cursor)];
			});

			window = Array.fill(s.sampleRate*0.01, {|i| i}).curvelin(0, s.sampleRate*0.01, 0, 1, 4);
			array = array.collect({|item, i| var value = window[i]; if(value.isNil, {item}, {value*item})});

			array.put(array.size-1, 0);
			file = Buffer.loadCollection(s, array);
			s.sync;
			writePath = folders ++ micNumber ++  "/imp_ls" ++ lsNumber ++ "-mic" ++ micNumber ++ ".wav";
			postf("Writing: %\n", writePath);
			file.write(writePath, "wav");
			written = i+1;
		});
	};
	postf("Written: % files\n", written);
}
)


