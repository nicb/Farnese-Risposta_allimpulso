# Farnese - Risposta all'impulso

Collezione di risposte all'impulso del teatro Farnese, Palazzo della Pilotta, Parma

## Sinossi

Questo deposito contiene le risposte all'impulso realizzate mediante _sweep
esponenziale_ utilizzando il _DFM_ (_Definitive Farina Method_) descritto
[qui](./DFM.md).

Gli sweep sono stati emessi da ciascun altoparlante distribuito in  sala
per il _Prometeo, Tragedia dell'ascolto_  di  Luigi  Nono  eseguito  nei
giorni 26/27/28 maggio 2017. La campagna di raccolta si è svolta il 23 maggio 2017
alle ore 22:00.

## Licenza

Tutto il materiale contenuto in questo deposito è licenziato con licenza GNU
GPL 3.0. La descrizione dettagliata della licenza si trova [qui](./LICENSE).
