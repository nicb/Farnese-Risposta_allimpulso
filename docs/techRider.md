#Technical Rider

##mics:
4 Nuemann km184
1 DPA 4090

##loudspeakers:

*ls1/8 ---> cluster 3* d&b T10
(3 e 4 raddoppiati su E8 (fronte palco orchestra 4))

##software:

*impulses recorded on pro Tools (sr: 96000 bit depth: int24)
*sweep generata in SC 3.6.6 (link to script)
*Convolution module on FSCAPE 1.3.1 (link to software)
*editing (filtering) ADOBE Audition CS6

##final file format:

.wav 48000Hz int24